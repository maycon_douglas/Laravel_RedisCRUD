<?php 

namespace App\RedisCRUD;

use Illuminate\Database\Eloquent\Model;
use App\RedisCRUD\RedisCRUDFacade as RedisCRUD;

Class RedisCRUDModel extends Model { 

	public function redisSetTable($table){
		RedisCRUD::setTable($table);
	}

	public function redisSetPK($pk){
		RedisCRUD::setPK($pk);
	}

	public function redisSetAttr($attr){
		RedisCRUD::setAttr($attr);
	}

	public function redisGetKey(){
		return RedisCRUD::getKey();
	}

	public function redisAdd($val){
		return RedisCRUD::add($val);	
	}

	public function redisGet(){
		return RedisCRUD::get();
	}

	public function redisUpd($val){
		return RedisCRUD::upd($val);
	}

	public function redisDel(){
		return RedisCRUD::del();
	}
}