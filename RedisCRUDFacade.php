<?php 

namespace App\RedisCRUD;

use Redis;

abstract class RedisCRUDFacade {
	
    private static $table = "";
    private static $pk = "";
    private static $attr = "";
    private static $key = "";

    public static function setTable($table){
        self::$table = $table;
        self::makeKey();
    }

    public static function setPK($pk){
        self::$pk = $pk;
        self::makeKey();
    }

    public static function setAttr($attr){
        self::$attr = $attr;
        self::makeKey();
    }    

	private static function makeKey(){
        self::$key = self::$table."_".self::$pk."_".self::$attr;
    }

    public static function getKey(){
        return self::$key;
    }

    public static function add($val){
        if(!empty(self::getKey() ) and !Redis::exists(self::getKey() ) ){
    		Redis::set(self::getKey(), $val );
    		return true;
		}else{
			return false;
		}

    } 

    public static function get(){

        if(!empty(self::getKey() ) and Redis::exists(self::getKey() ) ){
            return Redis::get(self::getKey() );
        }else{
            return false;
        }   
    }

    
    public static function upd($val){
    	if(!empty(self::getKey() ) and Redis::exists(self::getKey() ) ){
    		Redis::set(self::getKey(), $val );
    		return true;
		}else{
			return false;
		}
    }

    public static function del(){
    	if(!empty(self::getKey() ) and Redis::exists(self::getKey() ) ){
    		Redis::del(self::getKey() );
    		return true;
		}else{
			return false;
		}
    }



}