# RedisCRUD

> ## Após instalar e configurar o Redis no laravel, adicione esta duas classe no diretorio App/RedisCRUD/  em seu projeto e ao invés de extender a classe Model extenda a RedisCRUDModel que tem todas as funcionalidades da Model original mais as funcionalidades de um crud com redis. Você ainda consegue usar tranquilamente a facade Redis.

> ## O banco de dados Redis organiza seus dados em forma de chave:valor

> ## Para ficar mais organizado eu coloquei na ordem: Tabela => Chave Primaria => Campo. Usando um '_' entre os dados.

exemplo:

```javascript
	"pessoasTable_666_nome" : "antiCristo"
	
	"pessoasTable_666_idade" : "22"

	"carrosTable_1_placa" : "abc123"
	
	"carrosDonosTable_666|1_cor" : "azul" //chave composta, vc e livre para separar os campos da forma que achar melhor
```

## Atributos:	

```php
	private $key => chave do Redis, por padrao nula
```	

## Argumentos:

```php
	mixed $pk => chave primaria da tabela

	mixed $attr => atributo da tabela

	mixed $val => valor para ser gravado
```

## Funções:

```php	
	public void redisSetPK( $pk) => define a chave primaria 

	public void redisSetAttr( $attr ) => define o campo 

	public mixed  redisGetKey() => pega o valor da key

	bool redisAdd( $val ) => criar novo campo no Redis, retorna true caso tenha criado e falso caso já exista

	public mixed redisGet() => pega o campo no Redis, retorna o campo caso exista e false caso não exista
	
	public bool redisUpd( $val ) => atualiza um campo no Redis, retorna  true caso tenha atualizado e false caso não exista

	public bool redisDel() => deleta um campo no Redis, retorna true caso seja deletado e false caso não exista
```	

## exemplo simples:

```php
<?php
//model

namespace App;

use App\RedisCRUD\RedisCRUDModel as Model;

class Guitarra extends Model{
	
	function __construct (){
		parent::__construct ();
		$this->setTable("guitarras"):
	}
}

//Controller

namespace App;

use App\Guitarra;

class GuitarraController extends Controller{
	
	function postCadastrar(Guitarra $guitarra, Request $request){
		$guitarra->modelo = $request->modelo;
		$guitarra->preco =  $request->preco;
		$guitarra->fabricante = $request->fabricante;
		$guitarra->quantidade = $request->quantidade;

		if($guitarra->save() ){
			$guitarra->redisSetPK($guitarra->codigo);
			$guitarra->redisSetAttr("modelo");
			$guitarra->redisAdd($guitarra->modelo);

			$guitarra->redisSetAttr("preco");
			$guitarra->redisAdd($guitarra->preco);

			$guitarra->redisSetAttr("fabricante");
			$guitarra->redisAdd($guitarra->fabricante);

			$guitarra->redisSetAttr("quantidade");
			$guitarra->redisAdd($guitarra->quantidade);

			return "cadastrado no cache";
		}else{
			return "nao foi possivel cadastar";
		}
	}

	function getVer($id, $attr, Guitarra $guitarra){
		$guitarra->redisSetPK($id);
		$guitarra->redisSetAttr($attr);
		return $guitarra->redisGet();
	}
}

```